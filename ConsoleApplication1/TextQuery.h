#ifndef TEXTQUERY_H
#define TEXTQUERY_H

#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <fstream>
#include <sstream>
#include <memory>
#include <tuple>

#include "MyMemory.h"

using std::string;

typedef std::tuple<string, MySharedPtr<std::set<size_t>>, MySharedPtr<std::vector<string>>> QueryResultTuple;

std::ostream &print(std::ostream &os, const QueryResultTuple &qr);

class QueryResult;

class TextQuery
{
	friend class QueryResult;
public:
	using line_no = std::vector<std::string>::size_type;

	TextQuery(std::ifstream &input);
	TextQuery() : file(new std::vector<std::string>) {}

	//QueryResult query(const string &word) const;
	QueryResultTuple query(const string &word) const;
protected:
	//std::shared_ptr<std::vector<string>> file;
	//std::map<string, std::shared_ptr<std::set<line_no>>> wm;
	MySharedPtr<std::vector<string>> file;
	std::map<string, MySharedPtr<std::set<line_no>>> wm;
};

class TextQuerySentences : public TextQuery
{
	friend class QueryResult;
public:
	TextQuerySentences(std::ifstream &input);
};

class QueryResult
{
public:
	typedef std::set<size_t>::const_iterator line_it;

	size_t count;

	friend std::ostream &print(std::ostream &os, const QueryResult &qr);

	QueryResult(const string &word) : sought(word), count(0) {}
	/*QueryResult(const string &word, std::shared_ptr<std::set<size_t>> p,
		std::shared_ptr<std::vector<string>> f) :
		sought(word), lines(p), file(f), count(p->size()) {}*/
	QueryResult(const string &word, MySharedPtr<std::set<size_t>> p,
		MySharedPtr<std::vector<string>> f) :
		sought(word), lines(p), file(f), count(p->size()) {}

	line_it begin() const { return lines->cbegin(); }
	line_it end() const { return lines->cend(); }

	//std::shared_ptr<std::vector<std::string>> get_file() { return file; }
	MySharedPtr<std::vector<std::string>> get_file() { return file; }
private:
	string sought;
	/*std::shared_ptr<std::set<size_t>> lines;
	std::shared_ptr<std::vector<string>> file;*/
	MySharedPtr<std::set<size_t>> lines;
	MySharedPtr<std::vector<string>> file;
};

class Query;

class Query_base
{
	friend class Query;
protected:
	using line_no = TextQuery::line_no;
	virtual ~Query_base() = default;
private:
	//virtual QueryResult eval(const TextQuery &t) const = 0;
	virtual QueryResultTuple eval(const TextQuery &t) const = 0;
	virtual std::string rep() const = 0;

	virtual Query_base *clone() const & { return nullptr; }
	virtual Query_base *clone() && { return nullptr; }
};

class Query
{
	friend Query operator~(const Query &);
	friend Query operator|(const Query &l, const Query &r);
	friend Query operator&(const Query &l, const Query &r);
	friend Query operator&(Query &&l, Query &&r);
public:
	Query() = default;
	Query(const std::string &s);

	Query(const Query &query) : q(query.q->clone()) {}
	Query(Query &&query) : q(query.q->clone()) {}

	Query & operator=(const Query &query)
	{
		Query_base *tmp = query.q->clone();

		delete q;

		q = tmp;

		return *this;
	}

	Query & operator=(Query &&query) noexcept
	{
		if (this != &query)
		{
			using std::swap;

			delete q;

			swap(q, query.q);

			query.q = nullptr;
		}

		return *this;
	}

	~Query() { delete q; }

	//QueryResult eval(const TextQuery &t) const
	QueryResultTuple eval(const TextQuery &t) const
	{
		if (!q)
		{
			//return QueryResult("");
			QueryResultTuple qr;
			return qr;
		}

		return q->eval(t);
	}

	std::string rep() const
	{
		if (!q)
			return "empty query!";

		return q->rep();
	}

	bool empty() { return !q; }
private:
	//Query(std::shared_ptr<Query_base> query) : q(query) {}
	//std::shared_ptr<Query_base> q;
	Query(Query_base *query) : q(query) {}
	Query_base *q;
};

inline std::ostream & operator<<(std::ostream &os, const Query &query)
{
	return os << query.rep();
}

class WordQuery : public Query_base
{
	friend class Query;
	WordQuery(const std::string &s) : query_word(s) {}

	//QueryResult eval(const TextQuery &t) const override { return t.query(query_word); }
	QueryResultTuple eval(const TextQuery &t) const override { return t.query(query_word); }
	std::string rep() const override { return query_word; }

	virtual WordQuery *clone() const & override { return new WordQuery(*this); }
	virtual WordQuery *clone() && override { return new WordQuery(std::move(*this)); }

	std::string query_word;
};

inline Query::Query(const std::string &s) : q(new WordQuery(s)) {}

class NotQuery : public Query_base
{
	friend Query operator~(const Query &);
	NotQuery(const Query &q) : query(q) {}

	//QueryResult eval(const TextQuery &) const override;
	QueryResultTuple eval(const TextQuery &) const override;
	std::string rep() const override { return "~(" + query.rep() + ")"; }

	virtual NotQuery *clone() const & override { return new NotQuery(*this); }
	virtual NotQuery *clone() && override { return new NotQuery(std::move(*this)); }

	Query query;
};

class BinaryQuery : public Query_base
{
protected:
	BinaryQuery(const Query &l, const Query &r, std::string s) : lhs(l), rhs(r), opSym(s) {}

	std::string rep() const override { return "(" + lhs.rep() + " " + opSym + " " + rhs.rep() + ")"; }

	virtual BinaryQuery *clone() const & override { return nullptr; }
	virtual BinaryQuery *clone() && override { return nullptr; }

	Query lhs, rhs;
	std::string opSym;
};

class AndQuery : public BinaryQuery
{
	friend Query operator&(const Query &, const Query &);
	friend Query operator&(Query &&, Query &&);

	AndQuery(const Query &left, const Query &right) : BinaryQuery(left, right, "&") {}
	AndQuery(Query &&left, Query &&right) : BinaryQuery(left, right, "&") {}

	virtual AndQuery *clone() const & override { return new AndQuery(*this); }
	virtual AndQuery *clone() && override { return new AndQuery(std::move(*this)); }

	//QueryResult eval(const TextQuery &) const override;
	QueryResultTuple eval(const TextQuery &) const override;
};

class OrQuery : public BinaryQuery
{
	friend Query operator|(const Query &, const Query &);
	OrQuery(const Query &left, const Query &right) : BinaryQuery(left, right, "|") {}

	virtual OrQuery *clone() const & override { return new OrQuery(*this); }
	virtual OrQuery *clone() && override { return new OrQuery(std::move(*this)); }

	//QueryResult eval(const TextQuery &) const override;
	QueryResultTuple eval(const TextQuery &) const override;
};

inline Query operator~(const Query &operand)
{
	//return std::shared_ptr<Query_base>(new NotQuery(operand));
	return new NotQuery(operand);
}

inline Query operator|(const Query &lhs, const Query &rhs)
{
	//return std::shared_ptr<Query_base>(new OrQuery(lhs, rhs));
	return new OrQuery(lhs, rhs);
}

inline Query operator&(const Query &lhs, const Query &rhs)
{
	//return std::shared_ptr<Query_base>(new AndQuery(lhs, rhs));
	return new AndQuery(lhs, rhs);
}

inline Query operator&(Query &&lhs, Query &&rhs)
{
	//return std::shared_ptr<Query_base>(new AndQuery(lhs, rhs));
	return new AndQuery(std::move(lhs), std::move(rhs));
}



#endif