#ifndef STRING_H
#define STRING_H

#pragma warning(disable:4996) 

#include <iostream>
/*#include <memory>
#include <utility>
#include <initializer_list>*/
#include <algorithm>

class String
{
	friend std::ostream& operator<<(std::ostream &os, const String &s);
	friend bool operator==(const String &l, const String &r);
	friend bool operator!=(const String &l, const String &r);
	friend bool operator<(const String &l, const String &r);
public:
	String() : elements(nullptr), first_free(nullptr), cap(nullptr) {}
	String(const String &s);
	String(String &&s) noexcept;
	String& operator=(const String &s);
	String& operator=(String &&s) noexcept;
	~String();

	String(const char * ch);

	char &operator[](size_t n) { return elements[n]; }
	const char &operator[](size_t n) const { return elements[n]; }

	void push_back(const char &ch);

	size_t size() const;
	size_t capacity() const;
	char *begin() const;
	char *end() const;

	void reserve(const size_t &newcapacity);
	void resize(const size_t newsize);
	void resize(const size_t newsize, const char &s);
private:
	char *elements;
	char *first_free;
	char *cap;

	std::allocator<char> alloc;

	void chk_n_alloc();
	std::pair<char*, char*> alloc_n_copy(const char *beg, const char *end);
	void free();
	void reallocate(const size_t newcap = 0);

};

#endif