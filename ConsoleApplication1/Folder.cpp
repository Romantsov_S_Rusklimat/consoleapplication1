#include "Folder.h"

void Message::save(Folder &f)
{
	folders.insert(&f);
	f.addMsg(this);
}

void Message::remove(Folder &f)
{
	folders.erase(&f);
	f.remMsg(this);
}

void Message::debug_print()
{
	std::cerr << "Message:\n\t" << contents << std::endl;
	std::cerr << "Appears in " << folders.size() << " Folders:";
	for (auto &f : folders)
		std::cerr << f->name << "\t";

	std::cerr << std::endl;
}

void Folder::debug_print()
{
	std::cerr << "Folder " << name << " exists " << msgs.size() << " msgs:";
	
	for (auto &m : msgs)
		std::cerr << m->contents << "\t";

	std::cerr << std::endl;
}

void Message::add_to_Folders(const Message &m)
{
	for (auto f : m.folders)
		f->addMsg(this);
}

void Message::move_Folders(Message *m)
{
	folders = std::move(m->folders);

	for (auto f : folders)
	{
		f->remMsg(m);
		f->addMsg(this);
	}
	m->folders.clear();
}

Message::Message(const Message &m) : contents(m.contents), folders(m.folders)
{
	add_to_Folders(m);
}

void Message::remove_from_Folders()
{
	for (auto f : folders)
		f->remMsg(this);
}

Message::~Message()
{
	remove_from_Folders();
}

Message::Message(Message &&m) : contents(std::move(m.contents))
{
	move_Folders(&m);
}

Message & Message::operator=(Message &&m)
{
	if (this != &m)
	{
		remove_from_Folders();

		contents = std::move(m.contents);

		move_Folders(&m);
	}

	return *this;
}

Message& Message::operator=(const Message &m)
{
	remove_from_Folders();

	contents = m.contents;
	folders = m.folders;

	add_to_Folders(m);

	return *this;
}

void swap(Message &l, Message &r)
{
	using std::swap;

	for (auto f : l.folders)
		f->remMsg(&l);
	for (auto f : r.folders)
		f->remMsg(&r);

	swap(l.folders, r.folders);
	swap(l.contents, r.contents);

	for (auto f : l.folders)
		f->addMsg(&l);
	for (auto f : r.folders)
		f->addMsg(&r);
}

Folder::Folder(const Folder &f) : msgs(f.msgs), name(f.name)
{
	for (auto &m : msgs)
		m->addFldr(this);
}

Folder::~Folder()
{
	for (auto &m : msgs)
		m->remFldr(this);
}

Folder & Folder::operator=(const Folder &f)
{
	for (auto &m : msgs)
		m->remFldr(this);

	msgs = f.msgs;
	name = f.name;

	for (auto &m : msgs)
		m->addFldr(this);

	return *this;
}

void Folder::save(Message &m)
{
	msgs.insert(&m);

	m.addFldr(this);
}