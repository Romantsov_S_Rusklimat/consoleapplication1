#ifndef HASPTR_H
#define HASPTR_H

#include <string>
#include <iostream>

using std::string;

class HasPtr
{
public:
	HasPtr(const string &s = string()) : ps(new string(s)), i(0) { /*std::cout << "constr " << *ps << endl;*/ }
	HasPtr(const size_t size) : ps(new string(size, '0')), i(0) { /*std::cout << "constr " << *ps << endl;*/ }

	HasPtr(const HasPtr &hp) : ps(new string(*hp.ps)), i(hp.i) { /*std::cout << "constr copy " << *ps << endl;*/ }
	HasPtr(HasPtr &&hp) noexcept : ps(hp.ps), i(hp.i)
	{
		std::cout << "constr copy&& " << *ps << endl;

		hp.ps = nullptr;
	}

	~HasPtr()
	{
		//std::cout << "destruct " << ps << endl;

		delete ps;
	}

	HasPtr & operator=(const HasPtr &r)
	{
		//std::cout << "operator=& " << *r.ps;
		 
		i = r.i;

		auto tmp = new string(*r.ps);
		delete ps;
		ps = tmp;

		return *this;
	}

	/*HasPtr & operator=(HasPtr r)
	{
		//std::cout << "operator= " << *r.ps << endl;

		swap(*this, r);

		return *this;
	}*/

	HasPtr & operator=(HasPtr &&r) noexcept
	{
		//std::cout << "operator= " << *r.ps << endl;

		if (this != &r)
		{
			using std::swap;

			delete ps;

			ps = r.ps;
			swap(i, r.i);

			r.ps = nullptr;
		}

		return *this;
	}

	friend bool operator<(const HasPtr &l, const HasPtr &r);
	friend std::ostream& operator<<(std::ostream &os, const HasPtr &l);

	friend void swap(HasPtr &l, HasPtr &r);

	int i;
	string *ps;
};

inline void swap(HasPtr &l, HasPtr &r)
{
	using std::swap;

	//std::cout << "swap " << *l.ps << " -> " << *r.ps << std::endl;

	swap(l.ps, r.ps);
	swap(l.i, r.i);
}

bool operator<(const HasPtr &l, const HasPtr &r)
{
	return *l.ps < *r.ps;
}

std::ostream& operator<<(std::ostream &os, const HasPtr &l)
{
	os << *l.ps;
	return os;
}

#endif