#include "BinStrTree.h"

void BinStrTree::print(const char symbol) const
{
	for (size_t i = 0; i != *maxLevel; ++i)
	{
		root->printLevelValue(i, true, true, 2, true, *maxLevel, symbol, 0);

		if (i + 1 == *maxLevel)
			continue;

		root->printLevelLine(i, true, true, 2, symbol, 0);
	}
}

void TreeNode::printLevelValue(const size_t lvl, bool rightNode, bool leftNode, const size_t &width, bool localLeft, const size_t &maxLvl, const char &symbol, const size_t curLvl,
	bool emptyNode) const
{
	if ((emptyNode && curLvl == lvl) || (!emptyNode && count == lvl))
	{
		size_t num = 0, numMinus = 0;

		//if (!emptyNode)
		{
			if (maxLvl <= lvl + 2)
			{
				numMinus = 1;
			}
			else if (maxLvl <= lvl + 3)
			{
				numMinus = 3;
			}
			else// if (maxLvl > lvl + 3)
			{
				numMinus = 3;

				for (size_t i = lvl + 3; i < maxLvl; ++i)
				{
					numMinus = numMinus * 2 + 3;
				}
			}
		}

		if (numMinus)
			--numMinus;

		if (leftNode)
			num = width * (maxLvl - lvl - 1);
		else if (maxLvl == lvl + 1)
		{
			if (localLeft)
				num = width / 2;
			else
				num = static_cast<size_t>(width * 1.5);

			if (num >= numMinus)
				num -= numMinus;
			else
				num = 0;
		}
		else
		{
			num = static_cast<size_t>(width * 2.5);

			for (size_t i = lvl; i < maxLvl - 2; ++i)
			{
				num = num * 2 + 1;
			}

			num -= numMinus;
		}

		if (!emptyNode)
			cout << string(num, ' ') << value;
		else
			cout << string(num + width / 2, symbol);

		//if (!emptyNode)
		{
			if (maxLvl <= lvl + 2)
			{
				num = 1;
			}
			else if (maxLvl <= lvl + 3)
			{
				num = 3;
			}
			else
			{
				num = 3;

				for (size_t i = lvl + 3; i < maxLvl; ++i)
				{
					num = num * 2 + 3;
				}
			}

			--num;

			cout << string(num, (!emptyNode && right) ? '_' : ' ');
		}

		if (rightNode)
		{
			cout << endl;
		}
	}
	else
	{
		if (!emptyNode && left)
			left->printLevelValue(lvl, false, leftNode, width, true, maxLvl, symbol, count + 1);
		else if(!emptyNode)
			printLevelValue(lvl, false, leftNode, width, true, maxLvl, symbol, count + 1, true);
		else
			printLevelValue(lvl, false, leftNode, width, true, maxLvl, symbol, curLvl + 1, true);

		if (!emptyNode && right)
			right->printLevelValue(lvl, rightNode, false, width, false, maxLvl, symbol, count + 1);
		else
			printLevelValue(lvl, rightNode, false, width, false, maxLvl, symbol, curLvl + 1, true);
	}
}

void TreeNode::printLevelLine(const size_t lvl, bool rightNode, bool leftNode, const size_t &width, const char &symbol, const size_t curLvl,
	bool emptyNode) const
{
	if ((emptyNode && curLvl == lvl) || (!emptyNode && count == lvl))
	{
		size_t num = 0;

		if (leftNode)
		{
			num = width * (*maxLevel.lock() - lvl - 1);
			num = num <= 0 ? 0 : num - 1;
		}
		else if (*maxLevel.lock() == lvl + 2)
		{
			num = static_cast<size_t>(width * 1.5);
		}
		else
		{
			num = static_cast<size_t>(width * 3.5);

			for (size_t i = lvl; i < *maxLevel.lock() - 3; ++i)
			{
				num = num * 2 - 1;
			}
		}

		cout << string(num, symbol);

		if (!emptyNode && left)
			cout << "/";
		else
			cout << symbol;

		if (*maxLevel.lock() == lvl + 2)
		{
			num = 1;
		}
		else if (*maxLevel.lock() == lvl + 3)
		{
			num = 3;
		}
		else
		{
			num = 3;

			for (size_t i = lvl; i < *maxLevel.lock() - 3; ++i)
			{
				num = num * 2 + 3;
			}
		}

		cout << string(num, symbol);

		if (!emptyNode && right)
			cout << "\\";
		else
			cout << symbol;

		if (rightNode)
			cout << endl;
	}
	else
	{
		if (!emptyNode && left)
			left->printLevelLine(lvl, false, leftNode, width, symbol, count + 1);
		else if (!emptyNode)
			printLevelLine(lvl, false, leftNode, width, symbol, count + 1, true);
		else
			printLevelLine(lvl, false, leftNode, width, symbol, curLvl + 1, true);

		if (!emptyNode && right)
			right->printLevelLine(lvl, rightNode, false, width, symbol, count + 1);
		else
		{
			printLevelLine(lvl, rightNode, false, width, symbol, curLvl + 1, true);
		}
	}
}