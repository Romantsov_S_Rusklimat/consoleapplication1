#include "TextQuery.h"

#include <algorithm>
#include <iterator>

TextQuery::TextQuery(std::ifstream &input) : file(new std::vector<std::string>, MyDebugDelete())
{
	string str;

	size_t lineNumber = 0;

	while (std::getline(input, str))
	{
		std::istringstream is(str);

		file->push_back(str);

		while (is >> str)
		{
			string unuseLetters = ",./ \"?";
			std::set<string> unuseWords({ "a" });

			string::size_type pos = 0;

			while ((pos = str.find_first_of(unuseLetters, pos)) != string::npos)
			{
				str.erase(pos, 1);
			}

			for (auto &s : str)
			{
				s = tolower(s);
			}

			if (unuseWords.find(str) != unuseWords.cend())
				continue;

			auto &lines = wm[str];

			if (!lines)
				lines.reset(new std::set<size_t>);

			lines->insert(lineNumber);
		}

		++lineNumber;
	}
}

TextQuerySentences::TextQuerySentences(std::ifstream & input) : TextQuery()
{
	string str, strSentence;

	size_t sentenceNumber = 0;

	std::string::size_type pos = 0, prevPos = 0;

	while (input >> str)
	{
		strSentence += (strSentence.empty() ? "" : " ") + str;

		string unuseLetters = ",./ \"?";
		std::set<string> unuseWords({ "a" });

		string::size_type pos = 0;

		while ((pos = str.find_first_of(unuseLetters, pos)) != string::npos)
		{
			str.erase(pos, 1);
		}

		for (auto &s : str)
		{
			s = tolower(s);
		}

		if (unuseWords.find(str) != unuseWords.cend())
			continue;

		auto &lines = wm[str];

		if (!lines)
			lines.reset(new std::set<size_t>);

		lines->insert(sentenceNumber);

		string lettersEndOfSentecse = ".!?";

		if (lettersEndOfSentecse.find(strSentence.back()) != string::npos)
		{
			file->push_back(strSentence);
			++sentenceNumber;
			strSentence = "";
		}
	}

	file->push_back(strSentence);
}

//QueryResult TextQuery::query(const string & word) const
QueryResultTuple TextQuery::query(const string & word) const
{
	//static std::shared_ptr<std::set<line_no>> nodata(new std::set<line_no>);
	static MySharedPtr<std::set<line_no>> nodata(new std::set<line_no>);

	auto wln = wm.find(word);

	if (wln != wm.cend())
	{
		//return QueryResult(word, wln->second, file);
		return std::make_tuple(word, wln->second, file);
	}

	//return QueryResult(word, nodata, file);
	return std::make_tuple(word, nodata, file);
}

std::ostream &print(std::ostream &os, const QueryResult &qr)
{
	if (qr.count == 0)
	{
		os << "0 elements found\n";
		return os;
	}

	os << qr.sought << " occurs " << qr.count << " times\n";

	for (auto &lineNo : *qr.lines)
	{
		os << "(line " << lineNo << ") ";

		os << *(qr.file->begin() + lineNo) << std::endl;
	}

	return os;
}

std::ostream &print(std::ostream &os, const QueryResultTuple &qr)
{
	if (std::get<1>(qr)->size() == 0)
	{
		os << "0 elements found\n";
		return os;
	}

	os << std::get<0>(qr) << " occurs " << std::get<1>(qr)->size() << " times\n";

	for (auto &lineNo : *std::get<1>(qr))
	{
		os << "(line " << lineNo << ") ";

		os << *(std::get<2>(qr)->begin() + lineNo) << std::endl;
	}

	return os;
}

//QueryResult OrQuery::eval(const TextQuery &tq) const
QueryResultTuple OrQuery::eval(const TextQuery &tq) const
{
	auto right = rhs.eval(tq), left = lhs.eval(tq);

	//auto ret_lines = std::make_shared<std::set<line_no>>(left.begin(), left.end());
	auto ret_lines = MySharedPtr<std::set<line_no>>(new std::set<line_no>);
	//ret_lines->insert(left.begin(), left.end());
	ret_lines->insert(std::get<1>(left)->cbegin(), std::get<1>(left)->cend());

	//ret_lines->insert(right.begin(), right.end());
	ret_lines->insert(std::get<1>(right)->cbegin(), std::get<1>(right)->cend());

	//return QueryResult(rep(), ret_lines, left.get_file());
	return std::make_tuple(rep(), ret_lines, std::get<2>(left));
}

//QueryResult AndQuery::eval(const TextQuery &tq) const
QueryResultTuple AndQuery::eval(const TextQuery &tq) const
{
	auto right = rhs.eval(tq), left = lhs.eval(tq);

	//auto ret_lines = std::make_shared<std::set<line_no>>();
	auto ret_lines = MySharedPtr<std::set<line_no>>(new std::set<line_no>);


	//std::set_intersection(left.begin(), left.end(), right.begin(), right.end(),
	std::set_intersection(std::get<1>(left)->cbegin(), std::get<1>(left)->cend(),
		std::get<1>(right)->cbegin(), std::get<1>(right)->cend(),
		std::inserter(*ret_lines, ret_lines->begin()));

	//return QueryResult(rep(), ret_lines, left.get_file());
	return std::make_tuple(rep(), ret_lines, std::get<2>(left));
}

//QueryResult NotQuery::eval(const TextQuery & tq) const
QueryResultTuple NotQuery::eval(const TextQuery & tq) const
{
	auto result = query.eval(tq);

	//auto ret_lines = std::make_shared<std::set<line_no>>();
	auto ret_lines = MySharedPtr<std::set<line_no>>(new std::set<line_no>);

	//auto beg = result.begin(), end = result.end();
	auto beg = std::get<1>(result)->cbegin(), end = std::get<1>(result)->cend();

	//auto sz = result.get_file()->size();
	auto sz = std::get<2>(result)->size();

	for (size_t n = 0; n != sz; ++n)
	{
		if (beg == end || *beg != n)
			ret_lines->insert(n);
		else if (beg != end)
			++beg;
	}

	//return QueryResult(rep(), ret_lines, result.get_file());
	return std::make_tuple(rep(), ret_lines, std::get<2>(result));
}
