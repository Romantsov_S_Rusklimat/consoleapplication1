#ifndef BLOB_H
#define BLOB_H

#include <vector>
#include <iostream>
#include <memory>

template<typename> class Blob;
template<typename> class BlobPtr;
template<typename> class ConstBlobPtr;

template <typename T> bool operator==(const Blob<T>&, const Blob<T>&);
template <typename T> bool operator!=(const Blob<T> &l, const Blob<T> &r);
template <typename T> bool operator<(const Blob<T> &l, const Blob<T> &r);

template<typename T> 
class Blob
{
	friend bool operator==<T>(const Blob<T> &l, const Blob<T> &r);
	friend bool operator!=<T>(const Blob<T> &l, const Blob<T> &r);
	friend bool operator< <T>(const Blob<T> &l, const Blob<T> &r);

	friend class BlobPtr<T>;
	friend class ConstBlobPtr<T>;

public:
	typedef typename std::vector<T>::size_type size_type;

	Blob();
	Blob(const Blob<T> &r);

	Blob(std::initializer_list<T> il);

	template<typename It> Blob(It b, It e) : data(std::make_shared<std::vector<T>>(b, e)) {}

	Blob<T>& operator=(const Blob<T> &r);
	T& operator[](size_t n);
	const T& operator[](size_t n) const;

	size_type size() const { return data->size(); }
	bool empty() const { return data->empty(); }

	void push_back(const T &s) { data->push_back(s); }
	void pop_back();

	T &front();
	T &back();

	T &front() const;
	T &back() const;

	// interface to BlobPtr
	BlobPtr<T> begin();  // can't be defined until BlobPtr is
	BlobPtr<T> end();

	ConstBlobPtr<T> begin() const;
	ConstBlobPtr<T> end() const;
private:
	std::shared_ptr<std::vector<T>> data;

	void check(size_type i, const std::string &msg) const;
};

template<typename T>
Blob<T>::Blob() : data(std::make_shared<std::vector<T>>()) {}

template<typename T>
Blob<T>::Blob(const Blob &r) : data(std::make_shared<std::vector<T>>(*r.data)) {}

template<typename T>
Blob<T>::Blob(std::initializer_list<T> il) try : data(std::make_shared<std::vector<T>>(il))
{
	throw std::invalid_argument("123");
}
catch (const std::invalid_argument &e)
{
	std::cout << e.what() << std::endl;
}

template<typename T>
Blob<T>& Blob<T>::operator=(const Blob<T> &r)
{
	data = std::make_shared<std::vector<T>>(*r.data);

	return *this;
}

template<typename T>
T& Blob<T>::operator[](size_t n)
{
	return (*data)[n];
}

template<typename T>
const T& Blob<T>::operator[](size_t n) const
{
	return (*data)[n];
}

template<typename T>
void Blob<T>::check(size_type i, const std::string &msg) const
{
	if (i >= data->size())
		throw std::out_of_range(msg);
}

template<typename T>
T & Blob<T>::front()
{
	check(0, "Front on empty Blob");
	return data->front();
}

template<typename T>
T & Blob<T>::back()
{
	check(0, "Back on empty Blob");
	return data->back();
}

template<typename T>
T & Blob<T>::front() const
{
	check(0, "Front on empty const Blob");
	return data->front();
}

template<typename T>
T & Blob<T>::back() const
{
	check(0, "Back on empty const Blob");
	return data->back();
}

template<typename T>
void Blob<T>::pop_back()
{
	check(0, "Pop back on empty Blob");
	return data->pop_back();
}

template<typename T>
bool operator==(const Blob<T> & l, const Blob<T> & r)
{
	return *l.data == *r.data;
}

template<typename T>
bool operator!=(const Blob<T> & l, const Blob<T> & r)
{
	return !(l == r);
}

template<typename T>
bool operator<(const Blob<T> & l, const Blob<T> & r)
{
	return *l.data < *r.data;
}

template<typename T> bool operator==(const BlobPtr<T> &l, const BlobPtr<T> &r);
template<typename T> bool operator!=(const BlobPtr<T> &l, const BlobPtr<T> &r);
template<typename T> bool operator<(const BlobPtr<T> &l, const BlobPtr<T> &r);

// BlobPtr throws an exception on attempts to access a nonexistent element 
template<typename T>
class BlobPtr {
	friend bool eq(const BlobPtr&, const BlobPtr&);
	friend bool operator==<T>(const BlobPtr<T> &l, const BlobPtr<T> &r);
	friend bool operator!=<T>(const BlobPtr<T> &l, const BlobPtr<T> &r);
	friend bool operator< <T>(const BlobPtr<T> &l, const BlobPtr<T> &r);

	//friend BlobPtr operator+(const BlobPtr &l, const size_t i);
	//friend size_t operator-(const BlobPtr &l, const BlobPtr &r);
public:
	BlobPtr() : curr(0) { }
	BlobPtr(Blob<T> &a, size_t sz = 0) : wptr(a.data), curr(sz) { }

	char &operator[](size_t n) { return (*this)[n]; }

	const char &operator[](size_t n) const { return (*this)[n]; }

	BlobPtr &operator++();
	BlobPtr &operator--();
	BlobPtr operator++(int);
	BlobPtr operator--(int);
	T &operator*() const;
	T *operator->() const;

	BlobPtr& operator+=(const size_t i);
private:
	// check returns a shared_ptr to the vector if the check succeeds
	std::shared_ptr<std::vector<T>>
		check(std::size_t, const std::string&) const;

	// store a weak_ptr, which means the underlying vector might be deoyed
	std::weak_ptr<std::vector<T>> wptr;
	std::size_t curr;      // current position within the array
};

template<typename T>
class ConstBlobPtr {
	friend bool eq(const ConstBlobPtr&, const ConstBlobPtr&);
	friend bool operator==(const ConstBlobPtr &l, const ConstBlobPtr &r);
	friend bool operator!=(const ConstBlobPtr &l, const ConstBlobPtr &r);
	friend bool operator<(const ConstBlobPtr &l, const ConstBlobPtr &r);

	friend size_t operator-(const ConstBlobPtr &l, const ConstBlobPtr &r);
public:
	ConstBlobPtr() : curr(0) { }
	ConstBlobPtr(const ConstBlobPtr<T> &a, size_t sz = 0) : wptr(a.data), curr(sz) { }

	const char &operator[](size_t n) const { return (*this)[n]; }

	ConstBlobPtr &operator++();
	ConstBlobPtr &operator--();
	ConstBlobPtr operator++(int);
	ConstBlobPtr operator--(int);

	const T &operator*() const;
	const T *operator->() const;
private:
	// check returns a shared_ptr to the vector if the check succeeds
	std::shared_ptr<std::vector<T>>
		check(std::size_t, const T&) const;

	// store a weak_ptr, which means the underlying vector might be deoyed
	std::weak_ptr<std::vector<T>> wptr;
	std::size_t curr;      // current position within the array
};

/*
class PtrBlobPtr
{
public:
	PtrBlobPtr(BlobPtr &ptr) : p(&ptr) {}

	BlobPtr & operator*()
	{
		return *p;
	}

	BlobPtr * operator->()
	{
		return &this->operator*();
	}
private:
	BlobPtr * p;
};*/

template<typename T>
inline std::shared_ptr<std::vector<T>> ConstBlobPtr<T>::check(std::size_t i, const T &msg) const
{
	auto ret = wptr.lock();   // is the vector still around?
	if (!ret)
		throw std::runtime_error("unbound BlobPtr");

	if (i >= ret->size())
		throw std::out_of_range(msg);
	return ret; // otherwise, return a shared_ptr to the vector
}

template<typename T>
inline ConstBlobPtr<T> Blob<T>::begin() const
{
	return ConstBlobPtr<T>(*this);
}

template<typename T>
inline ConstBlobPtr<T> Blob<T>::end() const
{
	auto ret = ConstBlobPtr<T>(*this, data->size());
	return ret;
}

template<typename T>
inline std::shared_ptr<std::vector<T>> BlobPtr<T>::check(std::size_t i, const std::string &msg) const
{
	auto ret = wptr.lock();   // is the vector still around?
	if (!ret)
		throw std::runtime_error("unbound BlobPtr");

	if (i >= ret->size())
		throw std::out_of_range(msg);
	return ret; // otherwise, return a shared_ptr to the vector
}

// begin and end members for Blob
template<typename T>
inline BlobPtr<T> Blob<T>::begin()
{
	return BlobPtr<T>(*this);
}

template<typename T>
inline BlobPtr<T> Blob<T>::end()
{
	auto ret = BlobPtr<T>(*this, data->size());
	return ret;
}

// named equality operators for BlobPtr
template<typename T>
inline bool eq(const BlobPtr<T> &lhs, const BlobPtr<T> &rhs)
{
	auto l = lhs.wptr.lock(), r = rhs.wptr.lock();
	// if the underlying vector is the same 
	if (l == r)
		// then they're equal if they're both null or 
		// if they point to the same element
		return (!r || lhs.curr == rhs.curr);
	else
		return false; // if they point to difference vectors, they're not equal

}

template<typename T>
inline bool neq(const BlobPtr<T> &lhs, const BlobPtr<T> &rhs)
{
	return !eq(lhs, rhs);
}

template<typename T>
bool operator==(const BlobPtr<T> & l, const BlobPtr<T> & r)
{
	return l.curr == r.curr && *l.wptr.lock() == *r.wptr.lock();
}

template<typename T>
bool operator<(const BlobPtr<T> & l, const BlobPtr<T> & r)
{
	if (l.wptr.lock() != r.wptr.lock())
		throw std::invalid_argument("can't compare iterators");

	return l.curr < r.curr;
}

template<typename T>
BlobPtr<T> & BlobPtr<T>::operator+=(const size_t i)
{
	check(curr + i, "increment past end of BlobPtr");
	curr += i;
	return *this;
}

template<typename T>
BlobPtr<T> operator+(const BlobPtr<T> & l, const size_t i)
{
	BlobPtr res = l;
	res += i;

	return res;
}

template<typename T>
size_t operator-(const BlobPtr<T> & l, const BlobPtr<T> &r)
{
	if (l.wptr.lock() != r.wptr.lock())
		throw std::invalid_argument("can't compare iterators");

	return l.curr - r.curr;
}

template<typename T>
size_t operator-(const ConstBlobPtr<T> & l, const ConstBlobPtr<T> &r)
{
	if (l.wptr.lock() != r.wptr.lock())
		throw std::invalid_argument("can't compare iterators");

	return l.curr - r.curr;
}

template<typename T>
bool operator!=(const BlobPtr<T> & l, const BlobPtr<T> & r)
{
	return !(l == r);
}

template<typename T>
bool operator==(const ConstBlobPtr<T> & l, const ConstBlobPtr<T> & r)
{
	return l.curr == r.curr && *l.wptr.lock() == *r.wptr.lock();
}

template<typename T>
bool operator<(const ConstBlobPtr<T> & l, const ConstBlobPtr<T> & r)
{
	if (l.wptr.lock() != r.wptr.lock())
		throw std::invalid_argument("can't compare iterators");

	return l.curr < r.curr;
}

template<typename T>
bool operator!=(const ConstBlobPtr<T> & l, const ConstBlobPtr<T> & r)
{
	return !(l == r);
}

template<typename T>
BlobPtr<T> & BlobPtr<T>::operator++()
{
	check(curr, "increment past end of BlobPtr");
	++curr;
	return *this;
}

template<typename T>
BlobPtr<T> & BlobPtr<T>::operator--()
{
	--curr;
	check(curr, "decrement past begin of BlobPtr");
	return *this;
}

template<typename T>
BlobPtr<T> BlobPtr<T>::operator++(int)
{
	BlobPtr res = *this;

	++*this;

	return res;
}

template<typename T>
BlobPtr<T> BlobPtr<T>::operator--(int)
{
	BlobPtr res = *this;

	--*this;

	return res;
}

template<typename T>
T & BlobPtr<T>::operator*() const
{
	auto p = check(curr, "dereference past end");
	return (*p)[curr];
}

template<typename T>
T * BlobPtr<T>::operator->() const
{
	return &this->operator*();
}

template<typename T>
const T & ConstBlobPtr<T>::operator*() const
{
	auto p = check(curr, "dereference past end");
	return (*p)[curr];
}

template<typename T>
const T * ConstBlobPtr<T>::operator->() const
{
	return &this->operator*();
}
#endif
