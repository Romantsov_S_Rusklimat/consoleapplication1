/*
 * This file contains code from "C++ Primer, Fifth Edition", by Stanley B.
 * Lippman, Josee Lajoie, and Barbara E. Moo, and is covered under the
 * copyright and warranty notices given in that book:
 * 
 * "Copyright (c) 2013 by Objectwrite, Inc., Josee Lajoie, and Barbara E. Moo."
 * 
 * 
 * "The authors and publisher have taken care in the preparation of this book,
 * but make no expressed or implied warranty of any kind and assume no
 * responsibility for errors or omissions. No liability is assumed for
 * incidental or consequential damages in connection with or arising out of the
 * use of the information or programs contained herein."
 * 
 * Permission is granted for this code to be used for educational purposes in
 * association with the book, given proper citation if and when posted or
 * reproduced.Any commercial use of this code requires the explicit written
 * permission of the publisher, Addison-Wesley Professional, a division of
 * Pearson Education, Inc. Send your request for permission, stating clearly
 * what code you would like to use, and in what specific way, to the following
 * address: 
 * 
 * 	Pearson Education, Inc.
 * 	Rights and Permissions Department
 * 	One Lake Street
 * 	Upper Saddle River, NJ  07458
 * 	Fax: (201) 236-3290
*/ 

#include <iostream>
using std::istream; using std::ostream;

#include "Sales_data.h"

Sales_data::Sales_data(std::istream &is) : Sales_data()
{
	// read will read a transaction from is into this object
	read(is, *this);
}

Sales_data & Sales_data::operator=(const std::string & s)
{
	bookNo = s;

	return *this;
}

double 
Sales_data::avg_price() const {
	if (units_sold)
		return revenue/units_sold;
	else
		return 0;
}

// add the value of the given Sales_data into this object
Sales_data& 
Sales_data::combine(const Sales_data &rhs)
{
	units_sold += rhs.units_sold; // add the members of rhs into 
	revenue += rhs.revenue;       // the members of ``this'' object
	return *this; // return the object on which the function was called
}

Sales_data 
add(const Sales_data &lhs, const Sales_data &rhs)
{
	Sales_data sum = lhs;  // copy data members from lhs into sum
	sum.combine(rhs);      // add data members from rhs into sum
	return sum;
}

// transactions contain ISBN, number of copies sold, and sales price
istream&
read(istream &is, Sales_data &item)
{
	double price = 0;
	is >> item.bookNo >> item.units_sold >> price;
	item.revenue = price * item.units_sold;
	return is;
}

ostream&
print(ostream &os, const Sales_data &item)
{
	os << item.isbn() << " " << item.units_sold << " " 
	   << item.revenue << " " << item.avg_price();
	return os;
}

std::ostream&
operator<<(std::ostream& out, const Sales_data& s)
{
	out << s.isbn() << " " << s.units_sold << " "
		<< s.revenue << " " << s.avg_price();
	return out;
}

std::istream & operator>>(std::istream &is, Sales_data &item)
{
	double price = 0;
	is >> item.bookNo >> item.units_sold >> price;

	if (is)
		item.revenue = price * item.units_sold;
	else
		item = Sales_data();

	return is;
}

Sales_data operator+(const Sales_data & l, const Sales_data & r)
{
	Sales_data sum = l;
	sum += r;
	return sum;
}

Sales_data& Sales_data::operator+=(const Sales_data & r)
{
	if(isbn() != r.isbn())
		throw isbn_mismatch("can't compare iterators", isbn(), r.isbn());

	units_sold += r.units_sold;
	revenue += r.revenue;

	return *this;
}

bool operator==(const Sales_data & l, const Sales_data & r)
{
	return l.bookNo == r.bookNo && l.revenue == r.revenue &&
		l.units_sold == r.units_sold;
}

bool operator!=(const Sales_data & l, const Sales_data & r)
{
	return !(l == r);
}

/*
bool operator<(const Sales_data& sd1, const Sales_data& sd2)
{
	return sd1.isbn() < sd2.isbn();
}*/

size_t hasher(const Sales_data &sd)
{
	return std::hash<std::string>()(sd.isbn());
}

bool eqOp(const Sales_data &lhs, const Sales_data &rhs)
{
	return lhs.isbn() == rhs.isbn();
}

namespace std
{
	size_t hash<Sales_data>::operator() (const Sales_data &s) const
	{
		return hash<string>() (s.bookNo) ^ hash<unsigned>() (s.units_sold) ^
			hash<double>() (s.revenue);
	}
}
