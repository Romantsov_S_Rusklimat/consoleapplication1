#ifndef BINSTRTREE_H
#define BINSTRTREE_H

#include <string>
#include <iostream>

using std::string;
using std::cout;
using std::endl;

class TreeNode
{
public:
	TreeNode() : value(""), use(new size_t(1)), count(0) {}
	TreeNode(const string &s, const int cnt, const std::shared_ptr<size_t> ml) :
		value(s), count(cnt), maxLevel(ml), use(new size_t(1)) {}

	TreeNode(const TreeNode &TN) : value(TN.value), use(TN.use), maxLevel(TN.maxLevel),
	count(TN.count), left(TN.left), right(TN.right) { ++*use; }

	TreeNode(const TreeNode &TN, const std::shared_ptr<size_t> &ml) :
		value(TN.value), count(TN.count), maxLevel(ml), use(new size_t(1))
	{
		if (TN.left)
			left = new TreeNode(*TN.left, maxLevel.lock());

		if (TN.right)
			right = new TreeNode(*TN.right, maxLevel.lock());
	}

	~TreeNode()
	{
		//cout << "destruct " << value << endl;

		if (--*use == 0)
		{
			delete left;
			delete right;
			delete use;
		}
	}

	TreeNode & operator=(const TreeNode &r)
	{
		++*r.use;

		if (--*use == 0)
		{
			delete left;
			delete right;
			delete use;
		}

		value = r.value;
		left = r.left;
		right = r.right;
		count = r.count;
		maxLevel = r.maxLevel;
		use = r.use;

		return *this;
	}

	TreeNode *addNode(const string &s)
	{
		for (size_t i = 0; i != *maxLevel.lock(); ++i)
		{
			if (checkLvl(i))
			{
				auto node = addNodeValue(s, i);
				if (node)
					return node;
			}
		}

		return nullptr;
	}

	TreeNode * addNodeLeft(const string &s)
	{
		if(left)
			throw std::out_of_range("left node already exist");

		left = new TreeNode(s, count + 1, maxLevel.lock());

		if (count + 1 == *maxLevel.lock())
			++*maxLevel.lock();

		return left;
	}

	TreeNode * addNodeRight(const string &s)
	{
		if (right)
			throw std::out_of_range("right node already exist");

		right = new TreeNode(s, count + 1, maxLevel.lock());

		if (count + 1 == *maxLevel.lock())
			++*maxLevel.lock();

		return right;
	}

	bool haveLeft() { return left; }
	bool haveRight() { return right; }

	TreeNode& getLeft() const { return *left; }
	TreeNode& getRight() const { return *right; }

	void setValue(const string &s) { value = s; }

	void printLevelValue(const size_t lvl, bool rightNode, bool leftNode, const size_t &width, bool localLeft, const size_t &maxLvl, const char &symbol, const size_t crLvl,
		bool emptyNode = false) const;
	void printLevelLine(const size_t lvl, bool rightNode, bool leftNode, const size_t &width, const char &symbol, const size_t curLvl,
		bool emptyNode = false) const;

private:
	string value;
	size_t count;
	TreeNode *left;
	TreeNode *right;

	std::weak_ptr<size_t> maxLevel;

	size_t *use;

	TreeNode* addNodeValue(const string &s, const size_t lvl)
	{
		if (count == lvl)
		{
			if (!left)
			{
				return addNodeLeft(s);
			}
			else if (!right)
			{
				return addNodeRight(s);
			}
			else
				return nullptr;
		}
		else if (count <= lvl)
		{
			auto node = left->addNodeValue(s, lvl);
			if (!node)
				node = right->addNodeValue(s, lvl);
			return node;
		}
		else
			return nullptr;
	}

	bool checkLvl(const size_t lvl)
	{
		if (count == lvl)
		{
			if (!right)
				return true;

			return false;
		}
		else
			return right->checkLvl(lvl);
	}
};

class BinStrTree
{
public:
	BinStrTree(const string &s = string()) :
		root(new TreeNode(s, 0, maxLevel)), maxLevel(std::make_shared<size_t>(1)) {}

	BinStrTree(const BinStrTree &BST) :
		root(new TreeNode(*BST.root, maxLevel)), maxLevel(std::make_shared<size_t>(*BST.maxLevel)) {}

	TreeNode &getRoot() { return *root; }

	void print(const char symbol = ' ') const;

	~BinStrTree()
	{
		//cout << "destruct bin tree" << endl;

		delete root;
	}

	BinStrTree & operator=(const BinStrTree &r)
	{
		maxLevel = std::make_shared<size_t>(*r.maxLevel);

		auto nroot = new TreeNode(*r.root, maxLevel);

		delete root;

		root = nroot;

		return *this;
	}

private:
	std::shared_ptr<size_t> maxLevel;

	TreeNode *root;
};

#endif