#ifndef MYMEMORY_H
#define MYMEMORY_H

#include <functional>

class MyDebugDelete
{
public:
	MyDebugDelete(std::ostream &s = std::cerr, std::string str_par = "_") : os(s), str(str_par) {}

	template <typename T>
	void operator () (T* p) const
	{
		os << str << "Deletting ptr\n";

		delete p;
	}
private:
	std::ostream &os;
	std::string str;
};

template<typename> class MySharedPtr;

template <typename T> bool operator<(const MySharedPtr<T> &l, const MySharedPtr<T> &r);

template <typename T>
class MySharedPtr
{
	friend bool operator< <T>(const MySharedPtr<T> &l, const MySharedPtr<T> &r);
public:
	MySharedPtr() = default;
	MySharedPtr(T *p, std::function<void(T*)> f = nullptr) : ptr(p), del(f), use(new size_t(1)) {}

	MySharedPtr(const MySharedPtr &s) : ptr(s.ptr), del(s.del), use(s.use) { ++*use; }
	MySharedPtr(MySharedPtr &&s) noexcept : ptr(s.ptr), del(s.del), use(s.use)
	{
		s.ptr = nullptr;
		s.use = nullptr;
		s.del = nullptr;
	}

	MySharedPtr& operator=(const MySharedPtr &s)
	{
		ptr = s.ptr;
		del = s.del;

		use = s.use;

		++*use;

		return *this;
	}
	MySharedPtr& operator=(MySharedPtr &&s) noexcept
	{
		if (this != &s)
		{
			using std::swap;

			destruct();

			del = s.del;
			use = s.use;
			swap(ptr, s.ptr);

			s.ptr = nullptr;
			s.use = nullptr;
			s.del = nullptr;
		}

		return *this;
	}

	~MySharedPtr()
	{
		destruct();
	}

	T &operator*() const { return *ptr; }
	T *operator->() const { return &this->operator*(); }

	operator bool() const { return ptr; }

	size_t use_count() { return *use; }

	void reset(T *p, std::function<void(T*)> f = nullptr)
	{
		destruct();

		ptr = p;
		del = f;
		use = new size_t(1);
	}

private:
	void destruct()
	{
		if (use && --*use == 0)
		{
			if (del)
				del(ptr);
			else
				delete ptr;

			delete use;
		}
	}

	T *ptr;
	std::function<void(T*)> del;

	size_t *use;
};

template<typename T>
bool operator<(const MySharedPtr<T> & l, const MySharedPtr<T> & r)
{
	return *l < *r;
}

template <typename T, typename F = nullptr_t>
class MyUniquePtr
{
	//friend bool operator< <T>(const MyUniquePtr<T> &l, const MyUniquePtr<T> &r);
public:
	MyUniquePtr() = default;
	MyUniquePtr(T *p, F f) : ptr(p), del(f) {}
	MyUniquePtr(T *p) : MyUniquePtr(p, F()) {}

	MyUniquePtr(const MyUniquePtr &s) = delete;
	MyUniquePtr(MyUniquePtr &&s) noexcept : ptr(s.ptr), del(s.del)
	{
		s.ptr = nullptr;
		//s.del = nullptr;
	}

	MyUniquePtr& operator=(const MyUniquePtr &s) = delete;

	MyUniquePtr& operator=(MyUniquePtr &&s) noexcept
	{
		if (this != &s)
		{
			using std::swap;

			destruct();

			del = s.del;
			swap(ptr, s.ptr);

			s.ptr = nullptr;
			s.del = nullptr;
		}

		return *this;
	}

	~MyUniquePtr()
	{
		destruct();
	}

	T &operator*() const { return *ptr; }
	T *operator->() const { return &this->operator*(); }

	//operator bool() const { return ptr; }

	void reset(T *p = nullptr)
	{
		destruct();

		if(p)
			ptr = p;
	}

	T* release()
	{
		T *p = ptr;

		ptr = nullptr;
		//del = nullptr;

		return p;
	}

	F& get_deleter() { return del; }

private:
	void destruct()
	{
		if(ptr)
		//if (typename F)
			del(ptr);
		/*else
			delete ptr;*/
	}

	T *ptr;
	F del;
};

template<typename T, typename... Args>
MySharedPtr<T> MyMakeShared(Args&&...args)
{
	T *t = new T(std::forward<Args>(args)...);

	return MySharedPtr<T>(t);
}

#endif