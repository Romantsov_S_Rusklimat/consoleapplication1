#ifndef VEC_H
#define VEC_H

#pragma warning(disable:4996) 

#include <iostream>
#include <memory>
#include <utility>
#include <initializer_list>
#include <algorithm>

template<typename T> class Vec;

template<typename T> bool operator==(const Vec<T> &l, const Vec<T> &r);
template<typename T> bool operator!=(const Vec<T> &l, const Vec<T> &r);
template<typename T> bool operator<(const Vec<T> &l, const Vec<T> &r);

template<typename T>
class Vec
{
	friend bool operator==<T>(const Vec<T> &l, const Vec<T> &r);
	friend bool operator!=<T>(const Vec<T> &l, const Vec<T> &r);
	friend bool operator< <T>(const Vec<T> &l, const Vec<T> &r);
public:
	Vec() : elements(nullptr), first_free(nullptr), cap(nullptr) {}
	Vec(const Vec &sv);
	Vec(Vec &&sv) noexcept;
	Vec& operator=(const Vec &sv);
	Vec& operator=(Vec &&sv) noexcept;
	~Vec();

	Vec(std::initializer_list<T> il);

	Vec &operator=(std::initializer_list<T> il);
	Vec &operator[](size_t n) { return elements[n]; }
	const T &operator[](size_t n) const { return elements[n]; }

	void push_back(const T &s);
	void push_back(T &&s);

	void pop_back();

	template <typename ...Args>
	void emplace_back(Args&&... args);

	size_t size() const;
	size_t capacity() const;
	T *begin() const;
	T *end() const;

	T &back() const;
	T &front() const;

	void reserve(const size_t &newcapacity);
	void resize(const size_t newsize);
	void resize(const size_t newsize, const T &s);
	void shrink_to_fit();
private:
	T *elements;
	T *first_free;
	T *cap;

	std::allocator<T> alloc;

	void chk_n_alloc();
	std::pair<T*, T*> alloc_n_copy(const T *beg, const T *end);
	void free();
	void reallocate(const size_t newcap = 0);

};

template<typename T>
Vec<T>::Vec(const Vec & sv)
{
	auto newdata = alloc_n_copy(sv.begin(), sv.end());

	elements = newdata.first;

	first_free = cap = newdata.second;
}

template<typename T>
Vec<T>::Vec(Vec && sv) noexcept
	: elements(sv.elements), first_free(sv.first_free), cap(sv.cap)
{
	sv.elements = sv.first_free = sv.cap = nullptr;
}

template<typename T>
Vec<T> & Vec<T>::operator=(const Vec<T> & sv)
{
	auto data = alloc_n_copy(sv.begin(), sv.end());

	free();

	elements = data.first;

	first_free = cap = data.second;

	return *this;
}

template<typename T>
Vec<T> & Vec<T>::operator=(Vec<T> && sv) noexcept
{
	if (this != &sv)
	{
		free();

		elements = sv.elements;
		first_free = sv.first_free;
		cap = sv.cap;

		sv.elements = sv.first_free = sv.cap = nullptr;
	}

	return *this;
}

template<typename T>
Vec<T>::~Vec<T>()
{
	free();
}

template<typename T>
Vec<T>::Vec(std::initializer_list<T> il)
{
	reallocate(il.size());

	for (auto &s : il)
		push_back(s);

}

template<typename T>
Vec<T> & Vec<T>::operator=(std::initializer_list<T> il)
{
	auto data = alloc_n_copy(il.begin(), il.end());

	free();

	elements = data.first;

	first_free = cap = data.second;

	return *this;
}

template<typename T>
void Vec<T>::push_back(const T & s)
{
	chk_n_alloc();

	alloc.construct(first_free++, s);
}

template<typename T>
void Vec<T>::push_back(T && s)
{
	chk_n_alloc();

	alloc.construct(first_free++, std::move(s));
}

template<typename T>
void Vec<T>::pop_back()
{
	alloc.destroy(--first_free);
}

template<typename T>
template<typename ...Args>
void Vec<T>::emplace_back(Args&&... args)
{
	chk_n_alloc();

	alloc.construct(first_free++, std::forward<Args>(args)...);
}

template<typename T>
size_t Vec<T>::size() const
{
	return first_free - elements;
}

template<typename T>
size_t Vec<T>::capacity() const
{
	return cap - elements;
}

template<typename T>
T * Vec<T>::begin() const
{
	return elements;
}

template<typename T>
T * Vec<T>::end() const
{
	return first_free;
}

template<typename T>
T & Vec<T>::back() const
{
	return *(first_free - 1);
}

template<typename T>
T & Vec<T>::front() const
{
	return *elements;
}

template<typename T>
void Vec<T>::reserve(const size_t & newcapacity)
{
	if (newcapacity <= capacity())
		return;

	reallocate(newcapacity);
}

template<typename T>
void Vec<T>::resize(const size_t newsize)
{
	resize(newsize, T());
}

template<typename T>
void Vec<T>::resize(const size_t newsize, const T & s)
{
	if (newsize == size())
		return;
	else if (newsize < size())
	{
		size_t cnt = size() - newsize;

		while (first_free != elements + newsize)
			alloc.destroy(--first_free);
	}
	else
	{
		if (newsize > capacity())
			reallocate(newsize);

		for (size_t i = size(); i != newsize; ++i)
			alloc.construct(first_free++, s);
	}
}

template<typename T>
void Vec<T>::shrink_to_fit()
{
	if (size() == capacity())
		return;

	reallocate(size());
}

template<typename T>
void Vec<T>::chk_n_alloc()
{
	if (size() == capacity())
		reallocate();
}

template<typename T>
std::pair<T*, T*> Vec<T>::alloc_n_copy(const T * b, const T * e)
{
	auto data = alloc.allocate(e - b);

#ifdef LIST_INIT
	return { data, std::uninitialized_copy(b, e, data) };
#else
	return std::make_pair(data, std::uninitialized_copy(b, e, data));
#endif
}

template<typename T>
void Vec<T>::free()
{
	if (elements)
	{
		for (auto p = first_free; p != elements;)
			alloc.destroy(--p);

		alloc.deallocate(elements, cap - elements);
	}
}

template<typename T>
void Vec<T>::reallocate(const size_t newcap)
{
	size_t newcapacity;

	if (newcap == 0)
		newcapacity = size() ? 2 * size() : 1;
	else
		newcapacity = newcap;

	auto newdata = alloc.allocate(newcapacity);

	auto dest = newdata;

	auto elem = elements;

	for (size_t i = 0; i != size(); ++i)
	alloc.construct(dest++, std::move(*elem++));

	/*std::uninitialized_copy(std::make_move_iterator(begin()),
		std::make_move_iterator(end()), dest);*/

	free();

	auto s = size();

	elements = newdata;
	first_free = dest;// +s;

	cap = elements + newcapacity;
}

template<typename T>
bool operator==(const Vec<T> & l, const Vec<T> & r)
{
	if (l.size() != r.size())
		return false;

	auto rp = r.begin();

	for (auto p = l.begin(); p != l.end(); ++p)
	{
		if (*p != *rp)
			return false;

		++rp;
	}

	return true;
}

template<typename T>
bool operator<(const Vec<T> & l, const Vec<T> & r)
{
	auto rp = r.begin();

	for (auto p = l.begin(); p != l.end(); ++p)
	{
		if (*p > *rp)
			return false;

		if (rp + 1 == r.end())
		{
			if (p + 1 == l.end())
				return *p < *rp;
			else
				return false;
		}
		++rp;
	}

	return true;
}

template<typename T>
bool operator!=(const Vec<T> & l, const Vec<T> & r)
{
	return !(l == r);
}

#endif
