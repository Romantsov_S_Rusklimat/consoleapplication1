#include "String.h"

String::String(const String &s)
{
	auto newdata = alloc_n_copy(s.begin(), s.end());

	elements = newdata.first;

	first_free = cap = newdata.second;

	std::cout << "'constr copy' " << s << std::endl;
}

String::String(String && s) noexcept : elements(s.elements), first_free(s.first_free), cap(s.cap)
{
	s.elements = s.first_free = s.cap = nullptr;

	std::cout << "'constr copy&&' " << *elements << std::endl;
}

String & String::operator=(const String & s)
{
	auto data = alloc_n_copy(s.begin(), s.end());

	free();

	elements = data.first;

	first_free = cap = data.second;

	std::cout << "'operator=' " << s << std::endl;

	return *this;
}

String & String::operator=(String && sv) noexcept
{
	if (this != &sv)
	{
		free();

		elements = sv.elements;
		first_free = sv.first_free;
		cap = sv.cap;

		sv.elements = sv.first_free = sv.cap = nullptr;
	}

	return *this;
}

String::~String()
{
	free();
}

String::String(const char * ch) : elements(nullptr), first_free(nullptr), cap(nullptr)
{
	size_t size = 0;
	while (*ch != '\0')
	{
		++ch;
		++size;
	}

	reallocate(size);

	ch -= size;

	while (*ch != '\0')
		push_back(*ch++);

}

void String::push_back(const char &ch)
{
	chk_n_alloc();

	alloc.construct(first_free++, ch);
}

size_t String::size() const
{
	return first_free - elements;
}

size_t String::capacity() const
{
	return cap - elements;
}

char * String::begin() const
{
	return elements;
}

char * String::end() const
{
	return first_free;
}

void String::reserve(const size_t & newcapacity)
{
	if (newcapacity <= capacity())
		return;

	reallocate(newcapacity);
}

void String::resize(const size_t newsize)
{
	resize(newsize, ' ');
}

void String::resize(const size_t newsize, const char & s)
{
	if (newsize == size())
		return;
	else if (newsize < size())
	{
		size_t cnt = size() - newsize;

		while (first_free != elements + newsize)
			alloc.destroy(--first_free);
	}
	else
	{
		if (newsize > capacity())
			reallocate(newsize);

		for (size_t i = size(); i != newsize; ++i)
			alloc.construct(first_free++, s);
	}
}

void String::chk_n_alloc()
{
	if (size() == capacity())
		reallocate();
}

std::pair<char*, char*> String::alloc_n_copy(const char * b, const char * e)
{
	auto data = alloc.allocate(e - b);

#ifdef LIST_INIT
	return { data, std::uninitialized_copy(b, e, data) };
#else
	return std::make_pair(data, std::uninitialized_copy(b, e, data));
#endif
}

void String::free()
{
	if (elements)
	{
		for (auto p = first_free; p != elements;)
			alloc.destroy(--p);

		alloc.deallocate(elements, cap - elements);
	}
}

void String::reallocate(const size_t newcap)
{
	size_t newcapacity;

	if (newcap == 0)
		newcapacity = size() ? 2 * size() : 1;
	else
		newcapacity = newcap;

	auto newdata = alloc.allocate(newcapacity);

	auto dest = newdata;

	auto elem = elements;

	for (size_t i = 0; i != size(); ++i)
		alloc.construct(dest++, std::move(*elem++));

	free();

	elements = newdata;
	first_free = dest;
	cap = elements + newcapacity;
}

std::ostream & operator<<(std::ostream & os, const String & s)
{
	std::for_each(s.elements, s.first_free, [&](const char ch) {os << ch; });
	/*for (auto p = s.elements; p != s.first_free; ++p)
		os << *p;)*/

	return os;
}

bool operator==(const String & l, const String & r)
{
	if (l.size() != r.size())
		return false;

	auto rp = r.begin();

	for (auto p = l.begin(); p != l.end(); ++p)
	{
		if (*p != *rp)
			return false;

		++rp;
	}

	return true;
}

bool operator<(const String & l, const String & r)
{
	auto rp = r.begin();

	for (auto p = l.begin(); p != l.end(); ++p)
	{
		if (*p > *rp)
			return false;

		if (rp + 1 == r.end())
		{
			return *p < *rp;
		}
		++rp;
	}

	return true;
}

bool operator!=(const String & l, const String & r)
{
	return !(l == r);
}