#ifndef QUOTE_H
#define QUOTE_H

#include <string>
#include <set>
#include <iostream>

class Quote
{
public:
	Quote() = default;
	Quote(const std::string &book, double sales_price) : bookNo(book), price(sales_price) {}
	std::string isbn() const { return bookNo; };

	Quote(const Quote &r) : bookNo(r.bookNo), price(r.price) { std::cout << "constr copy Quote " << bookNo << std::endl; }
	Quote(Quote &&r) : bookNo(r.bookNo), price(r.price) { std::cout << "constr copy&& Quote " << bookNo << std::endl; }

	Quote & operator=(const Quote &r)
	{
		bookNo = r.bookNo;
		price = r.price;
		std::cout << "operator= Quote " << bookNo << std::endl;
		return *this;
	}
	Quote & operator=(Quote &&r)
	{
		bookNo = r.bookNo;
		price = r.price;
		std::cout << "operator=&& Quote " << bookNo << std::endl;
		return *this;
	}

	virtual ~Quote() { std::cout << "destruct Quote " << bookNo << std::endl; }

	virtual Quote *clone() const & { return new Quote(*this); }
	virtual Quote *clone() && { return new Quote(std::move(*this)); }

	virtual double net_price(size_t n) const { return n * price; }

	virtual void debug(bool printEndl = true)
	{
		std::cout << bookNo << " " << price;
		if(printEndl)
			std::cout << std::endl;
	}
private:
	std::string bookNo;
protected:
	double price = 0.0;
};

class Disc_quote : public Quote
{
public:
	Disc_quote() = default;
	Disc_quote(const std::string &book, double p, size_t qty, double disc) : Quote(book, p), quantity(qty), discount(disc) {}

	Disc_quote(const Disc_quote &r) : Quote(r), quantity(r.quantity), discount(r.discount) { std::cout << "constr copy Disc_quote " << isbn() << endl; }
	Disc_quote(Disc_quote &&r) : Quote(std::move(r)), quantity(r.quantity), discount(r.discount) { std::cout << "constr copy&& Disc_quote " << isbn() << endl; }

	Disc_quote & operator=(const Disc_quote &r)
	{
		Quote::operator=(r);
		quantity = r.quantity; 
		discount = r.discount;
		std::cout << "operator= Disc_quote " << isbn() << std::endl;
		return *this;
	}
	Disc_quote & operator=(Disc_quote &&r)
	{
		Quote::operator=(std::move(r));
		quantity = r.quantity;
		discount = r.discount;
		std::cout << "operator=&& Disc_quote " << isbn() << std::endl;
		return *this;
	}

	~Disc_quote() { std::cout << "destruct Disc_quote " << isbn() << std::endl; }

	double net_price(size_t cnt) const = 0;

	void debug(bool printEndl = true)
	{
		Quote::debug(false);

		std::cout << " " << quantity << " " << discount;
		if (printEndl)
			std::cout << std::endl;
	}
protected:
	size_t quantity = 0;
	double discount = 0.0;
};

class Bulk_quote : public Disc_quote
{
public:
	Bulk_quote() = default;
	Bulk_quote(const std::string &book, double p, size_t qty, double disc) : Disc_quote(book, p, qty, disc) {}

	virtual Bulk_quote *clone() const & { return new Bulk_quote(*this); }
	virtual Bulk_quote *clone() && { return new Bulk_quote(std::move(*this)); }

	double net_price(size_t cnt) const override
	{
		if (cnt >= quantity)
			return cnt * (1 - discount) * price;
		else
			return cnt * price;

	}

	void debug(bool printEndl = true)
	{
		Quote::debug(false);

		std::cout << " " << quantity << " " << discount;
		if (printEndl)
			cout << std::endl;
	}
};

class Bulk_quoteLimited : public Disc_quote
{
public:
	Bulk_quoteLimited() = default;
	Bulk_quoteLimited(const std::string &book, double p, size_t qty, double disc, size_t max_q) : Disc_quote(book, p, qty, disc), max_qty(max_q) {}

	double net_price(size_t cnt) const override
	{
		if (cnt >= quantity)
			if(cnt > max_qty)
				return max_qty * (1 - discount) * price + (cnt - max_qty) * price;
			else
				return cnt * (1 - discount) * price;
		else
			return cnt * price;
	}

	void debug(bool printEndl = true)
	{
		Disc_quote::debug(false);
		std::cout << " " << max_qty << std::endl;
	}
private:
	size_t max_qty = 0;
};

double print_total(std::ostream &os, const Quote &item, size_t n)
{
	double ret = item.net_price(n);

	os << "ISBN: " << item.isbn()
		<< " #sold: " << n << " total due: " << ret << std::endl;

	return ret;
}

class Basket
{
public:
	void add_item(const std::shared_ptr<Quote> &sale) { items.insert(sale); }
	void add_item(const Quote &sale) { items.insert(std::shared_ptr<Quote>(sale.clone())); }
	void add_item(const Quote &&sale) { items.insert(std::shared_ptr<Quote>(std::move(sale).clone())); }

	double total_receipt(std::ostream &os) const
	{
		double sum = 0.0;

		for (auto iter = items.cbegin(); iter != items.cend(); iter = items.upper_bound(*iter))
		{
			sum += print_total(os, **iter, items.count(*iter));
		}

		os << "Total sale: " << sum << std::endl;

		return sum;
	}
private:
	static bool compare(const std::shared_ptr<Quote> &l, const std::shared_ptr<Quote> &r)
	{
		return l->isbn() < r->isbn();
	}

	std::multiset<std::shared_ptr<Quote>, decltype(compare)*> items{ compare };
};

#endif