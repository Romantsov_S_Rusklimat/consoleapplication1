/*
 * This file contains code from "C++ Primer, Fifth Edition", by Stanley B.
 * Lippman, Josee Lajoie, and Barbara E. Moo, and is covered under the
 * copyright and warranty notices given in that book:
 *
 * "Copyright (c) 2013 by Objectwrite, Inc., Josee Lajoie, and Barbara E. Moo."
 *
 *
 * "The authors and publisher have taken care in the preparation of this book,
 * but make no expressed or implied warranty of any kind and assume no
 * responsibility for errors or omissions. No liability is assumed for
 * incidental or consequential damages in connection with or arising out of the
 * use of the information or programs contained herein."
 *
 * Permission is granted for this code to be used for educational purposes in
 * association with the book, given proper citation if and when posted or
 * reproduced.Any commercial use of this code requires the explicit written
 * permission of the publisher, Addison-Wesley Professional, a division of
 * Pearson Education, Inc. Send your request for permission, stating clearly
 * what code you would like to use, and in what specific way, to the following
 * address:
 *
 * 	Pearson Education, Inc.
 * 	Rights and Permissions Department
 * 	One Lake Street
 * 	Upper Saddle River, NJ  07458
 * 	Fax: (201) 236-3290
*/

#ifndef SALES_DATA_H
#define SALES_DATA_H

#include <string>
#include <iostream>
#include <sstream>
#include <vector>

template<class T>
struct std::hash;

class Sales_data {
	friend Sales_data add(const Sales_data&, const Sales_data&);
	
	friend std::ostream &print(std::ostream&, const Sales_data&);
	friend std::istream &read(std::istream&, Sales_data&);
	
	friend std::ostream& operator<<(std::ostream&, const Sales_data&);
	friend std::istream& operator>>(std::istream &is, Sales_data &item);

	friend Sales_data operator+(const Sales_data &l, const Sales_data&r);

	friend bool operator==(const Sales_data &l, const Sales_data&r);

	friend bool compareUnits(const Sales_data &lhs, const Sales_data &rhs);

	friend struct std::hash<Sales_data>;
public:
	// constructors
// using the synthesized version is safe only
// if we can also use in-class initializers
#if defined(IN_CLASS_INITS) && defined(DEFAULT_FCNS)
	Sales_data() = default;
#else
	Sales_data() : units_sold(0), revenue(0.0) { }
#endif
#ifdef IN_CLASS_INITS
	Sales_data(const std::string &s) : bookNo(s) { }
#else
	/*explicit*/ Sales_data(const std::string &s) : Sales_data(s, 0, 0)
	{
		//std::cout << "constr 1 param" << std::endl;
	}
	Sales_data(const char *s) : units_sold(0), revenue(0)
	{
		std::string str;

		std::istringstream is(s);

		std::vector<std::string> svec;

		while (is >> str)
		{
			svec.push_back(str);
		}

		auto size = svec.size();

		if (size > 0)
		{
			bookNo = svec[0];
		}

		if (size > 1)
		{
			units_sold = std::stoi(svec[1]);
		}

		if (size > 2)
		{
			revenue = std::stod(svec[2]);
		}
	}
#endif
	Sales_data(const std::string &s, unsigned n, double p) :
		bookNo(s), units_sold(n), revenue(p*n)
	{
		//std::cout << "constr 3 param" << std::endl;
	}
	Sales_data(std::istream &is);
	~Sales_data() { /*std::cout << "Sales data destruct\n";*/ }
	
	Sales_data & operator=(const std::string &s);
	Sales_data& operator+=(const Sales_data&r);

	explicit operator std::string () const
	{
		return bookNo;
	}

	explicit operator double () const
	{
		return revenue;
	}

	// operations on Sales_data objects
	std::string isbn() const { return bookNo; }
	Sales_data& combine(const Sales_data&);
	double avg_price() const;
private:
	std::string bookNo;
#ifdef IN_CLASS_INITS   // using the synthesized version is safe only
	unsigned units_sold = 0;
	double revenue = 0.0;
#else
	unsigned units_sold;
	double revenue;
#endif
};


// nonmember Sales_data interface functions
Sales_data add(const Sales_data&, const Sales_data&);
std::ostream &print(std::ostream&, const Sales_data&);
std::istream &read(std::istream&, Sales_data&);

// used in future chapters
inline
bool compareUnits(const Sales_data &lhs, const Sales_data &rhs)
{
	return lhs.units_sold < rhs.units_sold;
}

size_t hasher(const Sales_data &sd);

bool eqOp(const Sales_data &lhs, const Sales_data &rhs);

namespace std
{
	template<>
	struct hash<Sales_data>
	{
		typedef size_t result_type;
		typedef Sales_data argument_type;

		size_t operator() (const Sales_data &s) const;
	};
}

class out_of_stock : public std::runtime_error
{
public:
	explicit out_of_stock(const std::string &s) : std::runtime_error(s) {}
};

class isbn_mismatch : public std::logic_error
{
public:
	explicit isbn_mismatch(const std::string &s) : std::logic_error(s) {}

	isbn_mismatch(const std::string &s, const std::string &l, const std::string &r) :
		std::logic_error(s), left(l), right(r) {}
	
	const std::string left, right;
};

#endif
